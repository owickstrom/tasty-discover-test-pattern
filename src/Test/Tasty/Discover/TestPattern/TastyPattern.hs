{-# LANGUAGE OverloadedStrings #-}
module Test.Tasty.Discover.TestPattern.TastyPattern where

import           Data.Text (Text)
import qualified Data.Text as Text

--------------------------------------------------------------------------------
-- * Tasty Patterns

data BinaryOperator = And | Or

data TastyPattern
  = BinaryOp BinaryOperator TastyPattern TastyPattern
  | SubgroupEquals Int Text

printBinaryOperator :: BinaryOperator -> Text
printBinaryOperator And = "&&"
printBinaryOperator Or  = "||"

printTastyPattern :: TastyPattern -> Text
printTastyPattern (BinaryOp op p1 p2) =
  "("
    <> printTastyPattern p1
    <> " "
    <> printBinaryOperator op
    <> " "
    <> printTastyPattern p2
    <> ")"
printTastyPattern (i `SubgroupEquals` t) =
  "($" <> Text.pack (show i) <> " == \"" <> t <> "\")"
