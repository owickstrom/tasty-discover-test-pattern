module Test.Tasty.Discover.TestPattern.Test where

import           Data.List.NonEmpty (NonEmpty (..))
import           Data.Text          (Text)

newtype Module = Module { subModuleNames :: NonEmpty Text }
  deriving (Eq, Show)

newtype TestName = TestName Text
  deriving (Eq, Show)

data Test = Test
  { testModule     :: Module
  , testDefinition :: TestName
  , testSpan       :: SrcSpan
  }
  deriving (Eq, Show)

data SrcSpan = SrcSpan { srcSpanStart :: SrcPos, srcSpanEnd :: SrcPos }
  deriving (Eq, Show)

data SrcPos = SrcPos { srcPosLine :: Int, srcPosColumn :: Int }
  deriving (Eq, Show)

instance Ord SrcPos where
  compare p1 p2 =
    case (srcPosLine p1 `compare` srcPosLine p2, srcPosColumn p1 `compare` srcPosColumn p2) of
      (LT, _)   -> LT
      (EQ, ord) -> ord
      (GT, _)   -> GT

isInside :: SrcPos -> SrcSpan -> Bool
isInside pos span' = pos >= srcSpanStart span' && pos <= srcSpanEnd span'
