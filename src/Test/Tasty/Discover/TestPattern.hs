{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ViewPatterns      #-}
module Test.Tasty.Discover.TestPattern where

import           Data.Coerce
import           Data.Function                                ((&))
import           Data.List
import           Data.List.NonEmpty                           (NonEmpty (..))
import qualified Data.List.NonEmpty                           as NonEmpty
import           Data.Text                                    (Text)
import qualified Data.Text                                    as Text
import qualified Language.Haskell.Exts                        as Haskell

import           Test.Tasty.Discover.TestPattern.TastyPattern
import           Test.Tasty.Discover.TestPattern.Test

-- * Pattern Construction

testName :: Test -> Text
testName =
  Text.replace "_" " "
    . Text.intercalate " "
    . tail
    . Text.splitOn "_"
    . coerce
    . testDefinition

toSpecificTestPattern :: Test -> TastyPattern
toSpecificTestPattern test = BinaryOp
  And
  (toModulePattern (testModule test))
  (SubgroupEquals (length (subModuleNames (testModule test)) + 2)
                  (coerce (testName test))
  )

toAllTestsPattern :: NonEmpty Test -> TastyPattern
toAllTestsPattern = foldl1 (BinaryOp Or) . fmap toSpecificTestPattern

toModulePattern :: Module -> TastyPattern
toModulePattern =
  foldl1 (BinaryOp And)
    . NonEmpty.zipWith SubgroupEquals (2 :| [3 ..])
    . subModuleNames

-- * Finding Tests

allInModule :: FilePath -> IO [Test]
allInModule modulePath = Haskell.parseFile modulePath >>= \case
  Haskell.ParseOk (Haskell.Module _ (Just moduleHead) _ _ decls) ->
    decls & foldMap (declToTest moduleHead) & nub & filter isTastyTest & pure
  Haskell.ParseOk _ -> fail $ "Not a parsable test module: " <> modulePath
  Haskell.ParseFailed srcLoc msg ->
    fail $ "Parsing failed at " <> Haskell.prettyPrint srcLoc <> ": " <> msg
 where
  declToTest moduleHead = \case
    Haskell.FunBind _ matches -> foldMap (funMatchToTest moduleHead) matches
    Haskell.PatBind srcSpan (Haskell.PVar _ name) _ _ ->
      pure (toTest moduleHead srcSpan name)
    _ -> mempty
  funMatchToTest moduleHead = \case
    Haskell.Match srcSpan name _ _ _ -> pure (toTest moduleHead srcSpan name)
    Haskell.InfixMatch srcSpan _ name _ _ _ ->
      pure (toTest moduleHead srcSpan name)
  toModule =
    Module
      . NonEmpty.fromList
      . Text.splitOn "."
      . Text.pack
      . Haskell.prettyPrint
  toTest (Haskell.ModuleHead _ moduleName _ _) srcSpan name = Test
    (toModule moduleName)
    (TestName (Text.pack (Haskell.prettyPrint name)))
    (toSrcSpan srcSpan)
  toSrcSpan (Haskell.srcInfoSpan -> span') = SrcSpan
    (SrcPos (Haskell.srcSpanStartLine span') (Haskell.srcSpanStartColumn span'))
    (SrcPos (Haskell.srcSpanEndLine span') (Haskell.srcSpanEndColumn span'))
  isTastyTest =
    (`elem` testPrefixes) . Text.takeWhile (/= '_') . coerce . testDefinition
  testPrefixes = ["prop", "scprop", "hprop", "unit", "spec", "test"]

testAtPositionInModule :: SrcPos -> FilePath -> IO (Maybe Test)
testAtPositionInModule pos modulePath =
  find ((pos `isInside`) . testSpan) <$> allInModule modulePath
