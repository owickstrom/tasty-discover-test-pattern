{-# LANGUAGE LambdaCase #-}
module Main where

import           Data.List.NonEmpty                           (nonEmpty)
import qualified Data.Text                                    as Text
import qualified Data.Text.IO                                 as Text
import qualified Data.Text.Read                               as Text
import           System.Environment
import           System.Exit
import           System.IO

import           Test.Tasty.Discover.TestPattern
import           Test.Tasty.Discover.TestPattern.TastyPattern
import           Test.Tasty.Discover.TestPattern.Test

failWith :: String -> IO ()
failWith msg = do
  hPutStrLn stderr msg
  exitFailure

usage :: IO ()
usage = hPutStrLn stderr "Usage: tasty-discover-run MODULE_PATH [LINE:COLUMN]"

main :: IO ()
main = getArgs >>= \case
  [modulePath] -> do
    tests <- allInModule modulePath
    case nonEmpty tests of
      Nothing     -> failWith ("No tests found in module: " <> modulePath)
      Just tests' -> Text.putStrLn (printTastyPattern (toAllTestsPattern tests'))
  [modulePath, srcPosStr] -> case parsePos (Text.pack srcPosStr) of
    Left  err -> failWith $ "Invalid source position: " <> err
    Right pos -> do
      foundTest <- testAtPositionInModule pos modulePath
      case foundTest of
        Just test -> Text.putStrLn (printTastyPattern (toSpecificTestPattern test))
        Nothing   -> failWith
          (  "No matching tests found in module "
          <> modulePath
          <> " at "
          <> srcPosStr
          )
  _ -> usage
 where
  parsePos t = case Text.splitOn (Text.singleton ':') t of
    [t1, t2] ->
      SrcPos <$> (fst <$> Text.decimal t1) <*> (fst <$> Text.decimal t2)
    _ -> Left $ "Invalid source position format: " <> Text.unpack t
