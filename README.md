# tasty-discover-test-pattern

Extract Tasty "patterns" for tests in Haskell modules, based on
[tasty-discover](https://hackage.haskell.org/package/tasty-discover)
naming conventions.

## Requirements

* A Tasty test suite constructed by `tasty-discover` using the
  `--tree-display` option.

## Usage

This tool is meant to be integrated into editors. It provides only a
way of constructing a Tasty "pattern" that can be used when running a
Tasty test suite.

There are currently two ways you can construct patterns:

1. patterns for running all tests in a module:
	 ```bash
	 tasty-discover-test-pattern test/Foo/BarTest.hs
	 ```
2. patterns for running a test at a specific location in a file:
	 ```bash
	 tasty-discover-test-pattern test/Foo/BarTest.hs 43:12
	 ```

To demonstrate how the patterns can be used from the command line,
even if they would be more useful in an editor plugin, we can set the
`TASTY_PATTERN` environment variable to the resulting pattern and run
the test suite:

```bash
TASTY_PATTERN="$(tasty-discover-test-pattern test/Foo/BarTest.hs)" cabal new-test
```

## License

[Mozilla Public Licence v2.0](LICENSE)
